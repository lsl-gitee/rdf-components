package com.lsl.rdf.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsl on 2021/6/11.
 */
@Data
@Component
@ConfigurationProperties(prefix = "rdf.user")
public class UserProperties {

    /**
     * 是否开启用户权限认证，默认开启
     */
    private boolean enableAuth = true;

    /**
     * api接口白名单，可配置指定白名单接口
     */
    private List<String> apiWhitelist = new ArrayList<>();

    /**
     * access token 有效期，单位为秒，默认7天
     */
    private long accessTokenExpires = 604800L;

    /**
     * refresh token 有效期，单位为秒，默认15天
     */
    private long refreshTokenExpires = 1296000L;

    /**
     * 是否使用snowflake id,默认使用自增，不使用snowflake id
     */
    private boolean useSnowflakeId = false;
}
