package com.lsl.rdf.annotation;

import java.lang.annotation.*;

/**
 * Created by lsl on 2021/4/7.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LockMethod {
    /**
     * el表达式
     * <br>
     * 通过el表达式，从LockBean对象获取对应的值，拼接成将要上锁的key值
     */
    String el() default "";

    /**
     * 等待锁等待
     */
    int waitSeconds() default 10;

    /**
     * 锁自动超时释放时间
     */
    int overtimeSeconds() default 120;

    /**
     * 清除一级缓存，避免二次确认时取到旧数据，导致锁失效，默认不清除
     */
    boolean clearCache() default false;

    /**
     * 是否是获取公平锁，默认非公平锁
     */
    boolean fairLock() default false;
}
