package com.lsl.rdf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lsl.rdf.BaseDo;
import com.lsl.rdf.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by lsl on 2021/6/4.
 */
public abstract class BaseServiceImpl<M extends BaseMapper<T>, T extends BaseDo> extends ServiceImpl<M, T> implements BaseService<T> {

    @Autowired
    private M mapper;

    public BaseServiceImpl() {
    }

    @Override
    public Integer insert(T insertDo) {
        return this.doInsert(insertDo);
    }

    private Integer doInsert(T insertDo) {
        return this.mapper.insert(insertDo);
    }

    @Override
    public Integer delete(T deleteDo) {
        return this.mapper.delete(new QueryWrapper<>(deleteDo));
    }

    @Override
    public Integer delete(QueryWrapper<T> queryWrapper) {
        return this.mapper.delete(queryWrapper);
    }

    @Override
    public Integer deleteById(Serializable id) {
        return this.mapper.deleteById(id);
    }

    private Integer doUpdate(T updateDo) {
        return this.mapper.updateById(updateDo);
    }

    @Override
    public Integer update(T updateDo) {
        return this.doUpdate(updateDo);
    }

    @Override
    public Integer count(QueryWrapper<T> queryWrapper) {
        return this.mapper.selectCount(queryWrapper);
    }

    @Override
    public Integer count(T countDo) {
        return this.mapper.selectCount(new QueryWrapper<>(countDo));
    }

    @Override
    public T selectOne(T selectDo) {
        return this.mapper.selectOne(new QueryWrapper<>(selectDo));
    }

    @Override
    public PageInfo<T> page(T pageDo) {
        this.setPage(pageDo.getPageBegin(), pageDo.getPageSize());
        List<T> list = this.mapper.selectList(new QueryWrapper<>(pageDo));
        return new PageInfo<>(list);
    }

    /**
     * 设置物理分页
     *
     * @param pageBegin 页码
     * @param pageSize  页大小
     */
    private void setPage(Integer pageBegin, Integer pageSize) {
        if (Objects.nonNull(pageBegin) && Objects.nonNull(pageSize)) {
            PageHelper.startPage(pageBegin, pageSize);
        } else {
            PageHelper.startPage(1, 10);
        }
    }

    @Override
    public PageInfo<T> page(T pageDo, QueryWrapper<T> queryWrapper) {
        this.setPage(pageDo.getPageBegin(), pageDo.getPageSize());
        List<T> list = this.mapper.selectList(queryWrapper);
        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<T> page(QueryWrapper<T> queryWrapper, Integer pageBegin, Integer pageSize) {
        if (Objects.isNull(pageBegin) || Objects.isNull(pageSize)) {
            pageBegin = 1;
            pageSize = 10;
        }
        this.setPage(pageBegin, pageSize);
        List<T> list = this.mapper.selectList(queryWrapper);
        return new PageInfo<>(list);
    }

    @Override
    @SuppressWarnings("all")
    public PageInfo<Map<String, Long>> pageMap(T pageDo, QueryWrapper<T> queryWrapper) {
        this.setPage(pageDo.getPageBegin(), pageDo.getPageSize());
        List<Map<String, Object>> maps = this.mapper.selectMaps(queryWrapper);
        return new PageInfo(maps);
    }

    @Override
    public List<T> list(QueryWrapper<T> queryWrapper, Integer pageBegin, Integer pageSize) {
        if (Objects.isNull(pageBegin) || Objects.isNull(pageSize)) {
            pageBegin = 1;
            pageSize = 50;
        }
        this.setPage(pageBegin, pageSize);
        List<T> list = this.mapper.selectList(queryWrapper);
        return list == null ? Collections.emptyList() : list;
    }
}
