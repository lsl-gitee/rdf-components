package com.lsl.rdf.utils.encrypt;

/**
 * Created by lsl on 2021/6/1.
 */
public class EncryptTest {

    public static void main(String[] args) throws Exception {
        System.out.println(Md5Util.MD5("123"));
        System.out.println(Md5Util.md5("123"));
        System.out.println(Md5Util.validate("123", Md5Util.MD5("123")));
        System.out.println(Md5Util.validate("234", Md5Util.MD5("123")));


        String str1 = AesUtil.encrypt("我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎", "1234511111111111");
        String decrypt = AesUtil.decrypt(str1, "1234511111111111");
        System.out.println(decrypt);

        long s2 = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            String str = AesUtil.encrypt("我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎我是警告我吉尔嘎", "1234511111111111");
            AesUtil.decrypt(str, "1234511111111111");
        }
        long e2 = System.currentTimeMillis();
        System.out.println(e2 - s2);

    }
}
