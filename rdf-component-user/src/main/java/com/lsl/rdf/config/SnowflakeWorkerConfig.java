package com.lsl.rdf.config;

import com.lsl.rdf.utils.SnowflakeIdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by lsl on 2021/6/11.
 */
@Slf4j
@Component
// 配置文件方式启用
@ConditionalOnProperty(prefix = "rdf.user", name = "useSnowflakeId", havingValue = "true")
public class SnowflakeWorkerConfig {

    private Random random = new Random();

    @Bean
    @ConditionalOnMissingBean(SnowflakeIdWorker.class)
    public SnowflakeIdWorker getSnowflakeId() {
        log.info("[RDF-COMPONENT-USER] SnowflakeIdWorker initialization completed.");
        return new SnowflakeIdWorker(random.nextInt(32), random.nextInt(32));
    }
}
