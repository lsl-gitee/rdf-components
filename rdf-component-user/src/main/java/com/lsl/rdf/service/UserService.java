package com.lsl.rdf.service;

import com.lsl.rdf.do_.UserDo;

/**
 * Created by lsl on 2021/4/9.
 */
public interface UserService extends BaseService<UserDo> {

    /**
     * 获取当前用户信息
     *
     * @return user info
     */
    UserDo currentUser();

    /**
     * 当前用户id
     *
     * @return user id
     */
    Long currentUserId();
}
