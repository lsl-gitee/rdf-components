package com.lsl.rdf.constant;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 通过用户名登录参数
 * <p>
 * Created by lsl on 2021/4/9.
 */
@Data
public class LoginParam {

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String mobile;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    private String password;
}
