package com.lsl.rdf.constant;

import lombok.Data;

import java.io.Serializable;

/**
 * 登录结果参数
 * <p>
 * Created by lsl on 2021/4/9.
 */
@Data
public class TokenResult implements Serializable {

    /**
     * 连接Token
     */
    private String accessToken;

    /**
     * token失效时间
     */
    private Long expireIn;

    /**
     * 刷新token
     */
    private String refreshToken;
}
