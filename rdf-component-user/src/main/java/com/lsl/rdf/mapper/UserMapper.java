package com.lsl.rdf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lsl.rdf.do_.UserDo;

/**
 * Created by lsl on 2021/4/9.
 */
public interface UserMapper extends BaseMapper<UserDo> {
}
