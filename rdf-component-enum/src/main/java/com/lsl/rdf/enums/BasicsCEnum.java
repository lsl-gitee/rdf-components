package com.lsl.rdf.enums;

import com.lsl.rdf.basics.BasicsEnum;

/**
 * Created by lsl on 2021/5/6.
 */
public class BasicsCEnum extends BasicsEnum {

    public BasicsCEnum(String code) {
        super(code);
    }

    public String getCode() {
        return super.getCode();
    }

    /**
     * 通过枚举码获取枚举对象
     *
     * @param enumClass 枚举类
     * @param code      枚举码
     * @return 枚举码对应的枚举对象
     */
    @SuppressWarnings("unchecked")
    public static <T extends BasicsEnum> T getByCode(Class<T> enumClass, String code) {
        return (T) BasicsEnum.getEnumByCode(enumClass, code);
    }
}
