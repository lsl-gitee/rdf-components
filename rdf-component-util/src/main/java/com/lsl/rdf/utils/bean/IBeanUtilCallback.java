package com.lsl.rdf.utils.bean;

/**
 * Created by lsl on 2021/6/1.
 */
@FunctionalInterface
public interface IBeanUtilCallback<S, T> {

    void callBack(S s, T t);
}
