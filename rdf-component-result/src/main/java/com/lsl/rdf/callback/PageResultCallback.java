package com.lsl.rdf.callback;

/**
 * Created by lsl on 2021/7/1.
 */
@FunctionalInterface
public interface PageResultCallback<S, T> {

    void callback(S source, T target);
}
