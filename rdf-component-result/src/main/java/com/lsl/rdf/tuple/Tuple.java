package com.lsl.rdf.tuple;

import java.io.Serializable;

/**
 * Created by lsl on 2021/2/25.
 */
public class Tuple<T> implements Serializable {
    private T first;

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public static <T> Tuple<T> of(T first) {
        Tuple<T> tuple = new Tuple<>();
        tuple.setFirst(first);
        return tuple;
    }

    @Override
    public String toString() {
        return "Tuple{" +
                "first=" + first +
                '}';
    }
}
