# rdf-component-enum

> 你是否因为每次新增枚举类都要添加几个获取值或者枚举的方法而苦恼，虽然都是复制粘贴而已，但还是无法忍受，该组件的存在就是为了解决这个问题。

## 一、效果
本组件提供了枚举的基本功能和一些常用的方法，同时还支持自动获取枚举描述并透出功能。

### 1. 基础用法
![基础用法](https://gitee.com/lsl-gitee/rdf-components/raw/optimize-basicsEnumSub/images/20210506153513.png "")

- 上图加粗字体内容为定义的枚举字段，上面的方法为该组件提供的方法，组件定义字段有 `val 值`、`code 码`和`desc 描述`。

- 方法说明

| 方法 | 说明 |
|--|--|
| getByVal | 通过枚举值获取枚举对象 |
| getByDesc | 通过枚举描述获取枚举对象 |
| getByCode | 通过枚举码获取枚举对象 |
| getValByDesc | 通过枚举描述获取枚举值 |
| getCodeByDesc | 通过枚举描述获取枚举码 |
| getDescByVal | 通过枚举值获取枚举描述 |
| getDescByCode | 通过枚举码获取枚举描述 |

### 2. 枚举描述自动透出
响应结果，`statusDesc` 字段内容的透出无需手动setter
```json
{
    "name": "rdf",
    "status": 1,
    "statusDesc": "点赞"
}
```

## 二、引入依赖
- 引入maven依赖
> 可直接在maven项目pom添加如下依赖，可以直接拉取改依赖，但前提是配置了本人Gitee私服仓库！[配置方式](https://gitee.com/lsl-gitee/imvn-repo/blob/master/README.md)
```xml
<dependencies>
    <dependency>
        <groupId>com.lsl.rdf</groupId>
        <artifactId>rdf-component-enum</artifactId>
        <!-- 最新版本 -->
        <version>1.0.2-RELEASE</version>
    </dependency>
</dependencies>
```
- 直接jar包引入
> 项目jar包本人已经打包部署到个人的私服仓库中了，可自行下载需要的jar及版本！[下载地址](https://gitee.com/lsl-gitee/imvn-repo/tree/master/com/lsl/rdf/rdf-component-enum)

## 三、使用方式
### 1. 基础枚举类定义于使用
1. 定义`VCD类型`枚举类，我们定义的是普通的枚举类，然后继承我们提供的枚举基类，这样我们就可以使用我们基类提供的方法了，具体定义方式如下：
```java
import com.lsl.rdf.enums.BasicsVCDEnum;

public class TypeEnum extends BasicsVCDEnum {
    public static final TypeEnum LIKE = new TypeEnum(1, "LIKE", "点赞");
    public static final TypeEnum COLLECT = new TypeEnum(2, "COLLECT", "收藏");
    public static final TypeEnum TRANSMIT = new TypeEnum(3, "TRANSMIT", "转发");
    public static final TypeEnum ATTENTION = new TypeEnum(4, "ATTENTION", "关注");

    public TypeEnum(Integer val, String code, String desc) {
        super(val, code, desc);
    }
}
```
2. 基本使用
```java
public class Test {

    public void test() {
        Integer val = TypeEnum.ATTENTION.getVal();
        String code = TypeEnum.ATTENTION.getCode();
        String desc = TypeEnum.ATTENTION.getDesc();

        TypeEnum attention = TypeEnum.getByCode(TypeEnum.class, "ATTENTION");
        TypeEnum attention1 = TypeEnum.getByDesc(TypeEnum.class, "关注");
        TypeEnum attention2 = TypeEnum.getByVal(TypeEnum.class, 4);

        String attention3 = TypeEnum.getCodeByDesc(TypeEnum.class, "关注");
        String attention4 = TypeEnum.getDescByCode(TypeEnum.class, "ATTENTION");
        String attention5 = TypeEnum.getDescByVal(TypeEnum.class, 4);
        Integer attention6 = TypeEnum.getValByDesc(TypeEnum.class, "关注");
    }
}
```
3. 说明
    - 什么叫`VCD类型`？`V`对应的是`val`、`C`对应的是`code`、`D`则对应的是`desc`，`VCD类型`表示提供这三个字段的支持，使用方式则是继承VCD的基类枚举类，
    这里我们提供了5种类型的枚举基类，可根据实际需要选择继承的枚举基类；
    
    | 枚举基类 | 支持字段 |
    |--|--|
    | BasicsVEnum | 仅支持 val |
    | BasicsCEnum | 仅支持 code |
    | BasicsVDEnum | 支持 val和desc |
    | BasicsCDEnum | 支持 code和desc |
    | BasicsVCDEnum | 支持 val、code和desc |
    
    - 枚举类字段需要定义为`public static final`，可参考上面的例子；
    - 使用枚举基类提供的方法，需要将枚举子类传入`TypeEnum.getByVal(TypeEnum.class, 4);`才能定位到子类对象。

### 2. 自动透出枚举描述
#### 1. 开启功能
该功能主要是基于AOP进行织入处理的，所以第一步需要先开启我们的AOP，开启方式有两种：
1. 通过Enable注解方式开启，直接在启动类添加`@EnableEnumDesc`。
```java
@SpringBootApplication
@EnableEnumDesc
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
```
启动时可看到`InjectEnumDescAop`初始化完成的日志
```log
2021-05-07 09:27:33.831  INFO 6952 --- [           main] com.lsl.rdf.config.InjectEnumDescConfig  : [RDF-COMPONENT-ENUM] InjectEnumDescAop initialization completed.
```

2. 通过配置文件创建AOP，新增配置类`ApplicationConfig`，添加我们AOP类的Bean即可。
```java
@Component
public class ApplicationConfig {
    @Bean
    public InjectEnumDescAop injectEnumDescAop() {
        return new InjectEnumDescAop();
    }
}
```

#### 2. 指定类于字段
```java
@InjectEnumTarget
public class ResultVo {

    private Long id;
    
    private String name;
    
    private String password;
    
    @InjectDescByVal(enumClass = StatusEnum.class)
    private Integer status;
    
    private String statusDesc;
    
    @InjectDescByVal(enumClass = TypeEnum.class, targetFieldName = "typeMsg")
    private Integer type;
    
    private String typeMsg;
    
    @InjectDescByCode(enumClass = StateEnum.class)
    private String state;
    
    private String stateDesc;
}
```
如上例子所示，`@InjectEnumTarget`注解用于指定需要织入的类，`@InjectDescByVal`用于指定枚举值对应的字段上，`enumClass`入参用于指定对应的枚举
类，`targetFieldName`入参用于指定用于装载枚举描述的字段，默认是枚举值字段加上`Desc`后缀，如果刚好匹配默认值则无需添加`targetFieldName`这个入
参，反之则需要指定字段名称，`@InjectDescByCode` 区别在于对应的枚举类是`CD`类型。

#### 3. 指定接口
```java
@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private UserMapper userMapper;

    @Override
    @InjectEnumResult
    public ResultVo selectOne(@LockTarget("id") Long id) {
        return userMapper.queryOne(id);
    }
}
```
最后我们还需要指定织入的接口，直接在接口的实现方法上添加`@InjectEnumResult`注解就可以了。

看下结果：
```json
{
    "id": 1,
    "name": "瞌睡虫",
    "password": null,
    "status": 1,
    "statusDesc": "点赞",
    "type": 2,
    "typeMsg": "关注",
    "state": "OK",
    "stateDesc": "成功"
}
```

#### 4. 说明
- 目前我们织入只支持简单的对象或者list，对于嵌套式的对象需要自动透出则需要自定义AOP才行，如：
```java
@InjectEnumTarget
public class A {
   @InjectDescByVal(enumClass = StatusEnum.class)
   private Integer status;
   
   private String statusDesc;
}

@InjectEnumTarget
public class B {

    @InjectDescByCode(enumClass = StateEnum.class)
    private String state;
    
    private String stateDesc;

    private A a;
}
```
这时透出B时，A对象里的字段是无法自动处理透出的。

### 3. 自定义织入AOP

你在用我们自动透出枚举描述功能的时候，可能会遇到一个问题，就是织入的接口的返回并不是一个简单的对象或者list，这时我们的织入就失效了，看下我们的AOP实现，
可以看到，目前只能针对我们自定义的返回封装体进行解析，如果是自定义的响应封装体，可能无法解析实现自动描述透出，这时我们有两种方法可以解决，如下：

```java
@Aspect
public class InjectEnumDescAop {

    @Pointcut("@annotation(com.lsl.rdf.annotation.InjectEnumResult)")
    public void pointCut() {
    }

    @AfterReturning(returning = "result", pointcut = "pointCut()")
    public Object afterReturn(Object result) {
        if (result instanceof SuccessResult) {
            Object data = ((SuccessResult) result).getData();
            if (data instanceof List) {
                EnumDescInjectUtil.invadeFieldsSetDesc((List<Object>) data);
            } else if (data instanceof PageResult) {
                List list = ((PageResult) data).getList();
                EnumDescInjectUtil.invadeFieldsSetDesc(list);
            } else if (data instanceof Tuple) {
                Object first = ((Tuple) data).getFirst();
                if (first instanceof List) {
                    EnumDescInjectUtil.invadeFieldsSetDesc((List<Object>) first);
                } else {
                    EnumDescInjectUtil.invadeFieldSetDesc(first);
                }
            } else {
                EnumDescInjectUtil.invadeFieldSetDesc(data);
            }
        }
        // 执行回调方法
        if (Objects.nonNull(injectEnumDescAopCallback)) {
            return injectEnumDescAopCallback.injectEnumDescCallback(result);
        }

        return result;
    }
}
```
#### 方法一、自定义AOP类
可以自定义如上的AOP类进行拦截处理，拦截`@InjectEnumResult`注解进行处理，同时加上`@Component`注解，使该bean生效，如下：

```java
@Aspect
@Component
public class CustomInjectEnumDescAop {

    @Pointcut("@annotation(com.lsl.rdf.annotation.InjectEnumResult)")
    public void pointCut() {
    }

    @AfterReturning(returning = "result", pointcut = "pointCut()")
    public Object afterReturn(Object result) {
        // 根据自己的实际情况编写逻辑，然后调用EnumDescInjectUtil提供的方法处理普通对象或者list对象
        if (result instanceof List) {
            EnumDescInjectUtil.invadeFieldsSetDesc((List<Object>) result);
        } else {
            EnumDescInjectUtil.invadeFieldSetDesc(result);
        }
        return result;
    }
}
```
**使用自定义的aop后，原来对原来的配置就可以去掉了，或者是启动注解`@EnableEnumDesc`**

#### 方法二、通过实现回调接口
我们定义了一个回调接口，可通过实现这个接口来达到执行自定义解析逻辑，定义一个类，然后实现`InjectEnumDescAopCallback`的
`injectEnumDescCallback`方法就可以执行自定义的逻辑了，使用方式如下：

```java
@Service
public class EnumDescCallbackImpl implements InjectEnumDescAopCallback {

    @Override
    public Object injectEnumDescCallback(Object result) {
        // 枚举透出自定义回调实现逻辑代码
        return result;
    }
}
```

## 四、联系作者
有发现有bug、建议或者有什么问题想问的，欢迎随时联系。

- QQ群：1083977145
- 邮箱：lsl.yx@foxmail.com

## 五、更多推荐
- [MybatisHelperPro](https://gitee.com/lsl-gitee/LDevKit/blob/feature-1.1.0/MybatisHelperPro)
> 可视化的Mybatis反编译工具，帮助你根据数据库表生成entity，dao接口和Mapper映射文件。

- [基于注解方式的分布式锁（redis)](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-lock/README.md)
> 基于注解实现的分布式锁，分布式锁原来可以这么简单！

## 六、最重要的
你可以借鉴使用到你的个人项目、商业项目、毕业设计等，但**不能二次开源！**

还有快点个 Star！Star！Star！