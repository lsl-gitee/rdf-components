package com.lsl.rdf;

/**
 * 业务异常
 * <p>
 * Created by lsl on 2021/2/25.
 */
public class BusinessException extends ExpectedException {

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }
}
