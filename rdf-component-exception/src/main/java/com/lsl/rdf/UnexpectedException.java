package com.lsl.rdf;

/**
 * 不可预知异常
 * <p>
 * Created by lsl on 2021/2/25.
 */
public class UnexpectedException extends BaseException {

    public UnexpectedException(String message) {
        super(message);
    }

    public UnexpectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnexpectedException(Throwable cause) {
        super(cause);
    }
}
