package com.lsl.rdf.lock;


import com.lsl.rdf.UnexpectedException;

/**
 * 锁等待超时异常
 * <p>
 * Created by lsl on 2021/4/7.
 */
public class LockWaitOverTimeException extends UnexpectedException {

    public LockWaitOverTimeException(String message) {
        super(message);
    }

    public LockWaitOverTimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public LockWaitOverTimeException(Throwable cause) {
        super(cause);
    }
}
