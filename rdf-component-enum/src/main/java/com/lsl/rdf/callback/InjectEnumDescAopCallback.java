package com.lsl.rdf.callback;

/**
 * 枚举描述自动透出回调方接口，可通过实现该接口自己实现枚举描述透出逻辑
 * <p>
 * Created by lsl on 2021/8/11.
 */
public interface InjectEnumDescAopCallback {

    /**
     * 枚举描述自动透出回调方接口
     *
     * @param result 接口返回原始对象
     * @return 解析枚举字段赋值了枚举描述后的返回对象
     */
    Object injectEnumDescCallback(Object result);
}
