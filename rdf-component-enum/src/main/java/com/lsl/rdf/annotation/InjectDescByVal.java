package com.lsl.rdf.annotation;

import com.lsl.rdf.basics.BasicsEnum;

import java.lang.annotation.*;

/**
 * 根据val 值注入描述注解
 * <p>
 * Created by lsl on 2021/3/15.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InjectDescByVal {

    /**
     * 指定获取描述的枚举类
     */
    Class<? extends BasicsEnum> enumClass();

    /**
     * 指定描述字段名称，将通过指定枚举类和枚举值获取枚举描述，默认 xxxDesc
     */
    String targetFieldName() default "";
}
