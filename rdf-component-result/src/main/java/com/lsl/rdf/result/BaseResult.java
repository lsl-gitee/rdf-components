package com.lsl.rdf.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lsl.rdf.errCode.ErrorCode;
import com.lsl.rdf.result.error.ErrorResult;
import com.lsl.rdf.result.success.SuccessResult;

import java.io.Serializable;

/**
 * Created by lsl on 2021/6/2.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings({"unchecked"})
public abstract class BaseResult<T> implements Serializable {

    private static final long serialVersionUID = -2733849641032268568L;

    /**
     * 请求成功响应子码
     */
    public static final Integer SUCCESS_CODE = 200;
    /**
     * 请求失败响应子码
     */
    public static final Integer BAD_REQUEST_CODE = 400;
    /**
     * 服务内部错误响应子码
     */
    public static final Integer BUSINESS_ERROR_CODE = 500;

    protected Long id;
    /**
     * 响应子码
     */
    protected Integer code;
    /**
     * 响应内容数据
     */
    protected T data;
    /**
     * 响应业务子码
     */
    protected String subCode;
    /**
     * 业务错误信息（可在前端展示）
     */
    protected String subMsg;
    /**
     * 系统错误信息（一般前端不做展示）
     */
    protected String msg;
    /**
     * 请求是否成功标识
     */
    protected Boolean success;

    public Long getId() {
        return id;
    }

    public Integer getCode() {
        return code;
    }

    public String getSubCode() {
        return subCode;
    }

    public String getSubMsg() {
        return subMsg;
    }

    public String getMsg() {
        return msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Boolean isSuccess() {
        return SUCCESS_CODE.equals(this.code);
    }

    public T getData() {
        return data;
    }

    public BaseResult() {
    }

    public static <T> BaseResult<T> success(T data) {
        return new SuccessResult(data);
    }

    public static <T> BaseResult<T> success() {
        return new SuccessResult();
    }

    public static <T> BaseResult<T> error(T data) {
        return new ErrorResult(data);
    }

    public static <T> BaseResult<T> error(T data, String subCode, String subMsg) {
        return new ErrorResult(data, subCode, subMsg);
    }

    public static <T> BaseResult<T> error(String subCode, String subMsg) {
        return new ErrorResult(subCode, subMsg);
    }

    public static <T> BaseResult<T> error(String subMsg) {
        return new ErrorResult(subMsg);
    }

    public static <T> BaseResult<T> error(ErrorCode errorCode) {
        return new ErrorResult(errorCode);
    }

    public static <T> BaseResult<T> error(ErrorCode errorCode, String msg) {
        return new ErrorResult(errorCode, msg);
    }

    protected static <T> BaseResult<T> error400(String msg) {
        ErrorResult result = new ErrorResult(ErrorCode.REQUEST_FAILED, msg);
        result.code = BAD_REQUEST_CODE;
        return result;
    }

    protected static <T> BaseResult<T> error400(String subMsg, String msg) {
        ErrorResult result = new ErrorResult(ErrorCode.REQUEST_FAILED.getCodeStr(), subMsg, msg);
        result.code = BAD_REQUEST_CODE;
        return result;
    }
}
