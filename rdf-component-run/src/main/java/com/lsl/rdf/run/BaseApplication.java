package com.lsl.rdf.run;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by lsl on 2021/6/17.
 */
@ComponentScan({
        "com.lsl.rdf"
})
public class BaseApplication {

    /**
     * 程序启动方法
     *
     * @param application 要启动的程序
     */
    protected static void ApplicationRun(Class<?> application) {
        new SpringApplication(application).run();
    }
}
