package com.lsl.rdf.do_;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lsl.rdf.BaseDo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * Created by lsl on 2021/6/10.
 */
@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@TableName("user")
public class UserDo extends BaseDo implements Serializable {
    /**
     * 用户名
     */
    @TableField("user_name")
    private String username;

    /**
     * 用户昵称
     */
    @TableField("nick_name")
    private String nickname;

    /**
     * 用户电话号码
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 密码加盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 用户头像地址
     */
    @TableField("avatar")
    private String avatar;
}