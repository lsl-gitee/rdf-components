package com.lsl.rdf;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体基础类
 * <p>
 * Created by lsl on 2021/6/4.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseDo implements Serializable {
    private static final long serialVersionUID = -1733849641032268568L;

    @TableId(value = "id", type = IdType.INPUT)
    protected Long id;

    @TableField(value = "create_date", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    protected Date createDate;

    @TableField(value = "update_date", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    protected Date updateDate;

    @TableField(value = "deleted", fill = FieldFill.INSERT)
    @TableLogic // 逻辑删
    protected Integer deleted;

    @TableField(value = "version", fill = FieldFill.INSERT)
    protected Integer version;

    /**
     * 页码
     */
    @TableField(exist = false) // mp忽略
    private Integer pageBegin;

    /**
     * 页大小
     */
    @TableField(exist = false)
    private Integer pageSize;
}
