package com.lsl.rdf.api;

import com.lsl.rdf.constant.*;
import com.lsl.rdf.result.BaseResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lsl on 2021/4/9.
 */
@RestController
public interface UserServiceRest {

    /**
     * 用户注册
     */
    @PostMapping("/user/v1/register")
    BaseResult<Boolean> userServiceRegisterRest(@RequestBody @Validated RegisterUserParam param);

    /**
     * 用户登录
     */
    @PostMapping("/user/v1/login")
    BaseResult<TokenResult> userServiceLoginRest(@RequestBody @Validated LoginParam param);

    /**
     * 刷新token
     */
    @PostMapping("/user/v1/refreshToken")
    BaseResult<TokenResult> userServiceRefreshTokenRest();

    /**
     * 用户登出
     */
    @PostMapping("/user/v1/logout")
    BaseResult<Boolean> userServiceLogoutRest();

    /**
     * 用户信息
     */
    @PostMapping("/user/v1/info")
    BaseResult<UserResult> userServiceInfoRest();

    /**
     * 更新用户信息
     */
    @PostMapping("/user/v1/update")
    BaseResult<Boolean> userServiceUpdateRest(@RequestBody UpdateUserParam param);
}
