package com.lsl.rdf.callback;

import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by lsl on 2021/6/22.
 */
public interface LogAdviceCallback {

    /**
     * 前置增强回调（同步）
     *
     * @param methodInvocation 方法调用
     */
    void syncBeforeAdvice(MethodInvocation methodInvocation);

    /**
     * 后置增强回调（同步）
     *
     * @param methodInvocation 方法调用
     * @param result           方法返回参数
     */
    void syncAfterAdvice(MethodInvocation methodInvocation, Object result);
}
