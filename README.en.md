# rdf-components

#### Introduce
Here we are going to share some of our own encapsulated common components that can be directly applied to your projects 
to simplify your code：①Annotation-based distributed lock、②Public enumeration component an so on,
other tools will be added in the future. Please welcome `Star` if you are interested。

star! star! star!

#### Components
- [Annotation-based distributed lock（redis)](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-lock/README.md)
- [Public enumeration component](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-enum/README.md)

#### Technical framework
- Foundation framework：Spring Boot 2.4.3.RELEASE
- Persistence layer frame：Mybatis Plus 3.4.2
- Cache：Redis
- Other: lombok 1.18.18、fastjson 1.2.76、redisson 3.15.3

#### Environment
- Language：Java 1.8.0_271
- IDE(Java)：IDEA
- Dependency Management：Maven
- Cache：Redis