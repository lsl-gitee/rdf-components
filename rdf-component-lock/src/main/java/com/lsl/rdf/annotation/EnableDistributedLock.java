package com.lsl.rdf.annotation;

import com.lsl.rdf.config.DistributedLockAopConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by lsl on 2021/4/30.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DistributedLockAopConfig.class)
public @interface EnableDistributedLock {
}
