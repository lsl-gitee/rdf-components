package com.lsl.rdf.enums;

import com.lsl.rdf.basics.BasicsEnum;

/**
 * Created by lsl on 2021/5/6.
 */
public class BasicsVDEnum extends BasicsEnum {

    public BasicsVDEnum(Integer val, String desc) {
        super(val, desc);
    }

    public Integer getVal() {
        return super.getVal();
    }

    public String getDesc() {
        return super.getDesc();
    }

    /**
     * 通过枚举描述获取枚举值
     *
     * @param enumClass 枚举类
     * @param desc      枚举描述
     * @return 枚举值
     */
    public static Integer getValByDesc(Class<? extends BasicsEnum> enumClass, String desc) {
        return BasicsEnum.getValByDesc(enumClass, desc);
    }

    /**
     * 通过枚举值获取枚举描述
     *
     * @param enumClass 枚举类
     * @param val       枚举值
     * @return 枚举描述
     */
    public static String getDescByVal(Class<? extends BasicsEnum> enumClass, Integer val) {
        return BasicsEnum.getDescByVal(enumClass, val);
    }

    /**
     * 通过枚举值获取枚举对象
     *
     * @param enumClass 枚举类
     * @param val       枚举值
     * @return 枚举值对应的枚举对象
     */
    @SuppressWarnings("unchecked")
    public static <T extends BasicsEnum> T getByVal(Class<T> enumClass, Integer val) {
        return (T) BasicsEnum.getEnumByVal(enumClass, val);
    }

    /**
     * 通过枚举描述获取枚举对象
     *
     * @param enumClass 枚举类
     * @param desc      枚举描述
     * @return 枚举描述对应的枚举对象
     */
    @SuppressWarnings("unchecked")
    public static <T extends BasicsEnum> T getByDesc(Class<T> enumClass, String desc) {
        return (T) BasicsEnum.getEnumByDesc(enumClass, desc);
    }
}
