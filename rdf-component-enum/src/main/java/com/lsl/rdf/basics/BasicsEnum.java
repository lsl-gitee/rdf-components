package com.lsl.rdf.basics;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * 基础枚举类，提供枚举常用的一些方法
 * <p>
 * Created by lsl on 2021/4/26.
 */
public abstract class BasicsEnum implements Serializable {

    /**
     * 键值
     */
    private Integer val;

    /**
     * 编码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    public BasicsEnum(String code) {
        this.code = code;
    }

    public BasicsEnum(Integer val) {
        this.val = val;
    }

    public BasicsEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public BasicsEnum(Integer val, String desc) {
        this.val = val;
        this.desc = desc;
    }

    public BasicsEnum(Integer val, String code, String desc) {
        this.code = code;
        this.val = val;
        this.desc = desc;
    }

    /**
     * 通过枚举描述获取枚举值
     *
     * @param clazz 枚举类
     * @param desc  枚举描述
     * @return 枚举值
     */
    protected static Integer getValByDesc(Class<? extends BasicsEnum> clazz, String desc) {
        BasicsEnum baseEnum = getEnumByDesc(clazz, desc);
        if (Objects.isNull(baseEnum)) {
            return null;
        }
        return baseEnum.val;
    }

    /**
     * 通过枚举描述获取枚举码
     *
     * @param clazz 枚举类
     * @param desc  枚举描述
     * @return 枚举码
     */
    protected static String getCodeByDesc(Class<? extends BasicsEnum> clazz, String desc) {
        BasicsEnum baseEnum = getEnumByDesc(clazz, desc);
        if (Objects.isNull(baseEnum)) {
            return null;
        }
        return baseEnum.code;
    }

    /**
     * 通过枚举值获取枚举描述
     *
     * @param clazz 枚举类
     * @param val   枚举值
     * @return 枚举描述
     */
    protected static String getDescByVal(Class<? extends BasicsEnum> clazz, Integer val) {
        BasicsEnum baseEnum = getEnumByVal(clazz, val);
        if (Objects.isNull(baseEnum)) {
            return null;
        }
        return baseEnum.desc;
    }

    /**
     * 通过枚举码获取枚举描述
     *
     * @param clazz 枚举类
     * @param code  枚举码
     * @return 枚举描述
     */
    protected static String getDescByCode(Class<? extends BasicsEnum> clazz, String code) {
        BasicsEnum baseEnum = getEnumByCode(clazz, code);
        if (Objects.isNull(baseEnum)) {
            return null;
        }
        return baseEnum.desc;
    }

    /**
     * 通过枚举值获取枚举对象
     *
     * @param clazz 枚举类
     * @param val   枚举值
     * @return 枚举值对应的枚举对象
     */
    protected static BasicsEnum getEnumByVal(Class<? extends BasicsEnum> clazz, Integer val) {
        Map<Integer, BasicsEnum> enumMap = EnumContainer.getClazzEnumValueMap(clazz);
        if (Objects.isNull(enumMap)) {
            return null;
        }
        return enumMap.get(val);
    }

    /**
     * 通过枚举描述获取枚举对象
     *
     * @param clazz 枚举类
     * @param desc  枚举描述
     * @return 枚举描述对应的枚举对象
     */
    protected static BasicsEnum getEnumByDesc(Class<? extends BasicsEnum> clazz, String desc) {
        Map<String, BasicsEnum> enumMap = EnumContainer.getClazzEnumDescMap(clazz);
        if (Objects.isNull(enumMap)) {
            return null;
        }
        return enumMap.get(desc);
    }

    /**
     * 通过枚举码获取枚举对象
     *
     * @param clazz 枚举类
     * @param code  枚举码
     * @return 枚举码对应的枚举对象
     */
    protected static BasicsEnum getEnumByCode(Class<? extends BasicsEnum> clazz, String code) {
        Map<String, BasicsEnum> enumMap = EnumContainer.getClazzEnumCodeMap(clazz);
        if (Objects.isNull(enumMap)) {
            return null;
        }
        return enumMap.get(code);
    }

    protected String getCode() {
        return code;
    }

    protected Integer getVal() {
        return val;
    }

    protected String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "BasicsEnum{" +
                "val=" + val +
                ", code='" + code + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
