package com.lsl.rdf.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by lsl on 2021/6/22.
 */
@Data
@Component
@ConfigurationProperties(prefix = "rdf.log")
public class LogProperties {
    /**
     * 是否开启日志
     */
    private boolean enableLog = true;

    /**
     * 日志打印格式
     */
    private String printLogPattern = "tid: {tid}] [app: {app}] [addr: {addr}:{port}] [reqIp: {reqIp}] [reqUrl: {reqUrl} | {method}] [reqMethod: {reqMethod}] [reqArgs: {reqArgs}] [respTime: {respTime}ms";

    /**
     * 日志pointCut表达式
     */
    private String logExpression;
}
