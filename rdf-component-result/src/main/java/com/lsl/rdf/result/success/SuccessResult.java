package com.lsl.rdf.result.success;

import com.lsl.rdf.result.BaseResult;

import java.io.Serializable;

/**
 * Created by lsl on 2021/6/2.
 */
@SuppressWarnings("rawtypes")
public class SuccessResult extends BaseResult implements Serializable {
    private static final long serialVersionUID = -2336441373804233324L;

    public SuccessResult(Object data) {
        this();
        this.data = data;
    }

    public SuccessResult() {
        this.msg = "成功";
        this.success = true;
        this.code = SUCCESS_CODE;
    }

    @Override
    public String toString() {
        return "SuccessResult{" +
                "id=" + id +
                ", code=" + code +
                ", data=" + data +
                ", subCode='" + subCode + '\'' +
                ", subMsg='" + subMsg + '\'' +
                ", msg='" + msg + '\'' +
                ", success=" + success +
                '}';
    }
}
