package com.lsl.rdf.config;

import com.lsl.rdf.aop.InjectEnumDescAop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by lsl on 2021/4/26.
 */
@Slf4j
@ConditionalOnWebApplication
public class InjectEnumDescConfig {

    @Bean
    public InjectEnumDescAop injectEnumDescAop() {
        log.info("[RDF-COMPONENT-ENUM] InjectEnumDescAop initialization completed.");
        return new InjectEnumDescAop();
    }
}
