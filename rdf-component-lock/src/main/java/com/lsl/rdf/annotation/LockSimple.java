package com.lsl.rdf.annotation;

import java.lang.annotation.*;

/**
 * Created by lsl on 2021/4/27.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LockSimple {
    /**
     * el表达式
     * <br>
     * 通过el表达式，从LockBean对象获取对应的值，拼接成将要上锁的key值
     */
    String el() default "";

    /**
     * 等待锁等待
     */
    int waitSeconds() default 10;

    /**
     * 锁自动超时释放时间
     */
    int overtimeSeconds() default 120;

    /**
     * 是否是获取公平锁，默认非公平锁
     */
    boolean fairLock() default false;
}
