package com.lsl.rdf.distributedLock;

/**
 * Created by lsl on 2021/4/7.
 */
public interface DistributedLock {

    /**
     * 请求锁
     *
     * @param lockName        锁名称
     * @param waitSeconds     请求锁等待时间
     * @param overtimeSeconds 锁超时时间
     * @return true 获得锁，false 未获得锁
     */
    boolean acquireLock(String lockName, int waitSeconds, int overtimeSeconds);

    /**
     * 请求锁（公平锁）
     *
     * @param lockName        锁名称
     * @param waitSeconds     请求锁等待时间
     * @param overtimeSeconds 锁超时时间
     * @return true 获得锁，false 未获得锁
     */
    boolean acquireFairLock(String lockName, int waitSeconds, int overtimeSeconds);

    /**
     * 释放锁
     *
     * @param lockName 释放锁名称
     */
    void releaseLock(String lockName);

    /**
     * 释放公平锁
     *
     * @param lockName 释放锁名称
     */
    void releaseFairLock(String lockName);
}
