package com.lsl.rdf.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lsl.rdf.BaseDo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by lsl on 2021/6/4.
 */
public interface BaseService<T extends BaseDo> extends IService<T> {

    Integer insert(T insertDo);

    Integer delete(T deleteDo);

    Integer delete(QueryWrapper<T> queryWrapper);

    Integer deleteById(Serializable id);

    /**
     * 根据Id更新
     */
    Integer update(T updateDo);

    /**
     * 统计记录条数
     */
    Integer count(QueryWrapper<T> queryWrapper);

    /**
     * 统计记录条数
     */
    Integer count(T countDo);

    /**
     * 根据条件检索数据，固定条件deleted=0
     */
    T selectOne(T selectDo);

    PageInfo<T> page(T pageDo);

    PageInfo<T> page(T pageDo, QueryWrapper<T> queryWrapper);

    PageInfo<T> page(QueryWrapper<T> queryWrapper, Integer pageBegin, Integer pageSize);

    PageInfo<Map<String, Long>> pageMap(T pageDo, QueryWrapper<T> queryWrapper);

    List<T> list(QueryWrapper<T> queryWrapper, Integer pageBegin, Integer pageSize);
}
