package com.lsl.rdf.config;

import com.lsl.rdf.distributedLock.DistributedLock;
import com.lsl.rdf.distributedLock.impl.DistributedLockImpl;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by lsl on 2021/6/17.
 */
@Slf4j
@Component
public class RdfUserApplicationConfig {

    private final RedisProperties redisProperties;

    public RdfUserApplicationConfig(RedisProperties redisProperties) {
        this.redisProperties = redisProperties;
    }

    @Bean
    @ConditionalOnMissingBean(RedissonClient.class)
    public RedissonClient redissonClient() {
        log.info("[RDF-COMPONENT-USER] RedissonClient initialization completed.");
        return new RedissonConfig(redisProperties).createSingle();
    }

    @Bean
    @ConditionalOnMissingBean(DistributedLock.class)
    public DistributedLock distributedLock() {
        log.info("[RDF-COMPONENT-USER] DistributedLock initialization completed.");
        return new DistributedLockImpl();
    }
}
