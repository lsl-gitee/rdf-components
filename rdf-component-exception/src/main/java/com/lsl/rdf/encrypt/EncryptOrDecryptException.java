package com.lsl.rdf.encrypt;

import com.lsl.rdf.UnexpectedException;

/**
 * Created by lsl on 2021/6/1.
 */
public class EncryptOrDecryptException extends UnexpectedException {

    public EncryptOrDecryptException(String message) {
        super(message);
    }

    public EncryptOrDecryptException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncryptOrDecryptException(Throwable cause) {
        super(cause);
    }
}
