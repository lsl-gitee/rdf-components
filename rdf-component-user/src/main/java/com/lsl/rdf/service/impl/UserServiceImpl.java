package com.lsl.rdf.service.impl;

import com.lsl.rdf.constant.enums.CommonConstants;
import com.lsl.rdf.do_.UserDo;
import com.lsl.rdf.mapper.UserMapper;
import com.lsl.rdf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Created by lsl on 2021/4/9.
 */
@Repository
public class UserServiceImpl extends BaseServiceImpl<UserMapper, UserDo> implements UserService {

    @Autowired
    private HttpServletRequest request;

    @Override
    public UserDo currentUser() {
        Long userId = (Long) request.getAttribute(CommonConstants.USER_ID_HEADER_ATTR);
        if (Objects.isNull(userId)) {
            return null;
        }
        return selectOne(UserDo.builder().id(userId).build());
    }

    @Override
    public Long currentUserId() {
        return currentUser().getId();
    }
}
