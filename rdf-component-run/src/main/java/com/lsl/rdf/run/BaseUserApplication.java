package com.lsl.rdf.run;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by lsl on 2021/8/4.
 */
@ComponentScan({
        "com.lsl.rdf"
})
@MapperScan({
        "com.lsl.rdf.mapper"
})
public class BaseUserApplication {

    /**
     * 程序启动方法
     *
     * @param application 要启动的程序
     */
    protected static void ApplicationRun(Class<?> application) {
        new SpringApplication(application).run();
    }
}
