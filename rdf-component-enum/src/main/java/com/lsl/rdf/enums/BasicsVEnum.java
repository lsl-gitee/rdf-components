package com.lsl.rdf.enums;

import com.lsl.rdf.basics.BasicsEnum;

/**
 * Created by lsl on 2021/5/6.
 */
public class BasicsVEnum extends BasicsEnum {

    public BasicsVEnum(Integer val) {
        super(val);
    }

    public Integer getVal() {
        return super.getVal();
    }

    /**
     * 通过枚举值获取枚举对象
     *
     * @param enumClass 枚举类
     * @param val       枚举值
     * @return 枚举值对应的枚举对象
     */
    @SuppressWarnings("unchecked")
    public static <T extends BasicsEnum> T getByVal(Class<T> enumClass, Integer val) {
        return (T) BasicsEnum.getEnumByVal(enumClass, val);
    }
}
