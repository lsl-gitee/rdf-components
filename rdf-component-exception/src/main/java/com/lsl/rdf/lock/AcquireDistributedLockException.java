package com.lsl.rdf.lock;


import com.lsl.rdf.UnexpectedException;

/**
 * 获取分布式锁异常
 * <p>
 * Created by lsl on 2021/4/7.
 */
public class AcquireDistributedLockException extends UnexpectedException {

    public AcquireDistributedLockException(String message) {
        super(message);
    }

    public AcquireDistributedLockException(String message, Throwable cause) {
        super(message, cause);
    }

    public AcquireDistributedLockException(Throwable cause) {
        super(cause);
    }
}
