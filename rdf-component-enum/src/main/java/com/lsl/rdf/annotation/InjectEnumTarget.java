package com.lsl.rdf.annotation;

import java.lang.annotation.*;

/**
 * 需要注入枚举描述的目标对象
 * <p>
 * Created by lsl on 2021/3/22.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InjectEnumTarget {
}
