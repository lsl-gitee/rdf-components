package com.lsl.rdf.constant;

import lombok.Data;

/**
 * 用户信息参数
 * <p>
 * Created by lsl on 2021/4/14.
 */
@Data
public class UpdateUserParam {

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户头像地址
     */
    private String avatar;
}
