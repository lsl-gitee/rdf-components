CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id，用户id',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '用户昵称',
  `mobile` varchar(20) DEFAULT NULL COMMENT '用户电话号码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `salt` varchar(50) DEFAULT NULL COMMENT '密码加盐',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像地址',
  `create_date` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(4) DEFAULT NULL COMMENT '逻辑删除 1删除 0未删除',
  `version` int(11) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_mobile_phone` (`mobile`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';