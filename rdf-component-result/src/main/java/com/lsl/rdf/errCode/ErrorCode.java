package com.lsl.rdf.errCode;

/**
 * Created by lsl on 2021/6/2.
 */
public enum ErrorCode {
    REQUEST_FAILED(40000, "请求错误"),
    // 500 类型错误
    SYSTEM_BUSY(50000, "系统繁忙"),
    UNKNOWN_SERVER_ERROR(50001, "未知错误"),
    ADDITION_FAILED(50002, "添加失败"),
    DELETE_FAILED(50003, "删除失败"),
    UPDATE_FAILED(50004, "更新失败"),
    QUERY_FAILED(50005, "查询失败"),
    NO_RESOURCE_AVAILABLE(50006, "没有此资源"),
    REQUEST_PARAMETER_ERROR(50007, "请求参数错误"),
    PERMISSION_DENIED(50008, "权限不足"),
    PAGE_NOT_FOUND(50009, "找不到页面"),
    ;
    private Integer code;
    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getCodeStr() {
        return code + "";
    }

    public String getMsg() {
        return msg;
    }

    ErrorCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
