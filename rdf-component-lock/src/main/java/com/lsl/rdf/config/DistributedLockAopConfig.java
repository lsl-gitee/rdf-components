package com.lsl.rdf.config;

import com.lsl.rdf.aop.DistributedLockAop;
import com.lsl.rdf.aop.SimpleDistributedLockAop;
import com.lsl.rdf.distributedLock.DistributedLock;
import com.lsl.rdf.distributedLock.impl.DistributedLockImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by lsl on 2021/4/30.
 */
@Slf4j
@ConditionalOnWebApplication
public class DistributedLockAopConfig {

    @Bean
    @ConditionalOnBean({RedissonClient.class})
    @ConditionalOnMissingBean(DistributedLock.class)
    public DistributedLock distributedLock() {
        log.info("[RDF-COMPONENT-LOCK] DistributedLock initialization completed.");
        return new DistributedLockImpl();
    }

    @Bean
    @ConditionalOnBean({DistributedLock.class})
    @ConditionalOnMissingBean(SimpleDistributedLockAop.class)
    public SimpleDistributedLockAop simpleDistributedLockAop() {
        log.info("[RDF-COMPONENT-LOCK] SimpleDistributedLockAop initialization completed.");
        return new SimpleDistributedLockAop();
    }

    @Bean
    @ConditionalOnBean({DistributedLock.class, SqlSessionFactory.class})
    @ConditionalOnMissingBean(DistributedLockAop.class)
    public DistributedLockAop distributedLockAop() {
        log.info("[RDF-COMPONENT-LOCK] DistributedLockAop initialization completed.");
        return new DistributedLockAop();
    }
}
