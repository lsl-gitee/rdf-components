package com.lsl.rdf.constant.enums;

/**
 * Created by lsl on 2021/6/16.
 */
public class CommonConstants {
    /**
     * access token header 属性名称
     */
    public static final String ACCESS_TOKEN_HEADER_ATTR = "access-token";
    /**
     * refresh token header 属性名称
     */
    public static final String REFRESH_TOKEN_HEADER_ATTR = "refresh-token";
    /**
     * 用户名默认前缀
     */
    public static final String USERNAME_PREFIX = "u";
    /**
     * request header user id 属性名
     */
    public static final String USER_ID_HEADER_ATTR = "USER_ID";

    /**
     * redis 用户信息缓存前缀
     */
    private static final String ACCESS_TOKEN_KEY = "RDF_USER_ACCESS_TOKEN:";
    private static final String REFRESH_TOKEN_KEY = "RDF_USER_REFRESH_TOKEN:";
    private static final String USER_TOKEN_KEY = "RDF_USER_TOKEN:";
    private static final String USER_TOKEN_LOCK_KEY = "RDF_USER_TOKEN_LOCK:";

    public static String getAccessTokenKey(String accessToken) {
        return ACCESS_TOKEN_KEY + accessToken;
    }

    public static String getRefreshTokenKey(String refreshToken) {
        return REFRESH_TOKEN_KEY + refreshToken;
    }

    public static String getUserTokenKey(Long userId) {
        return USER_TOKEN_KEY + userId;
    }

    public static String getUserTokenLockKey(Long userId) {
        return USER_TOKEN_LOCK_KEY + userId;
    }
}
