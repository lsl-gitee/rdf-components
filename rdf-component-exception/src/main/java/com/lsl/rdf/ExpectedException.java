package com.lsl.rdf;

/**
 * 可预知异常
 * <p>
 * Created by lsl on 2021/2/25.
 */
public class ExpectedException extends BaseException {

    public ExpectedException(String message) {
        super(message);
    }

    public ExpectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpectedException(Throwable cause) {
        super(cause);
    }
}
