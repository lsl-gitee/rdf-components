package com.lsl.rdf.service;

import com.lsl.rdf.constant.TokenResult;

/**
 * Created by lsl on 2021/4/15.
 */
public interface UserSupport {

    /**
     * 获取用户登录token
     *
     * @param uId 用户id
     * @return token info
     */
    TokenResult createToken(Long uId);

    /**
     * 刷新accessToken，对accessToken进行续期，refreshToken同样进行续期操作
     *
     * @return 新的token信息
     */
    TokenResult refreshToken();

    /**
     * 获取token的值
     *
     * @param accessToken token
     * @return uid
     */
    Long getToken(String accessToken);

    /**
     * 移除token
     *
     * @return true/false
     */
    Boolean removeToken();
}
