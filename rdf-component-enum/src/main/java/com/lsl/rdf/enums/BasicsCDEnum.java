package com.lsl.rdf.enums;

import com.lsl.rdf.basics.BasicsEnum;

/**
 * Created by lsl on 2021/5/6.
 */
public class BasicsCDEnum extends BasicsEnum {

    public BasicsCDEnum(String code, String desc) {
        super(code, desc);
    }

    public String getCode() {
        return super.getCode();
    }

    public String getDesc() {
        return super.getDesc();
    }

    /**
     * 通过枚举码获取枚举对象
     *
     * @param enumClass 枚举类
     * @param code      枚举码
     * @return 枚举码对应的枚举对象
     */
    @SuppressWarnings("unchecked")
    public static <T extends BasicsEnum> T getByCode(Class<T> enumClass, String code) {
        return (T) BasicsEnum.getEnumByCode(enumClass, code);
    }

    /**
     * 通过枚举描述获取枚举对象
     *
     * @param enumClass 枚举类
     * @param desc      枚举描述
     * @return 枚举描述对应的枚举对象
     */
    @SuppressWarnings("unchecked")
    public static <T extends BasicsEnum> T getByDesc(Class<T> enumClass, String desc) {
        return (T) BasicsEnum.getEnumByDesc(enumClass, desc);
    }

    /**
     * 通过枚举描述获取枚举码
     *
     * @param enumClass 枚举类
     * @param desc      枚举描述
     * @return 枚举码
     */
    public static String getCodeByDesc(Class<? extends BasicsEnum> enumClass, String desc) {
        return BasicsEnum.getCodeByDesc(enumClass, desc);
    }

    /**
     * 通过枚举码获取枚举描述
     *
     * @param enumClass 枚举类
     * @param code      枚举码
     * @return 枚举描述
     */
    public static String getDescByCode(Class<? extends BasicsEnum> enumClass, String code) {
        return BasicsEnum.getDescByCode(enumClass, code);
    }
}
