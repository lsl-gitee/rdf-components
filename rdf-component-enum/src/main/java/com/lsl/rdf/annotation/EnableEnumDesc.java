package com.lsl.rdf.annotation;

import com.lsl.rdf.config.InjectEnumDescConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by lsl on 2021/5/6.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(InjectEnumDescConfig.class)
public @interface EnableEnumDesc {
}
