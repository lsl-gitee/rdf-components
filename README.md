# rdf-components

#### 介绍
这里准备分享一些自己封装的公共组件，能够直接应用于你的项目，简化你的代码，目前规划分享的有：①基于注解的分布式锁、②公共枚举组件等，
后续还会增加其他工具，感兴趣的欢迎Star。

star! star! star!

#### 组件说明
- [基于注解方式的分布式锁（redis)](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-lock/README.md)
- [公共枚举组件](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-enum/README.md)

#### 模块说明
| 模块名称 | 说明 |
|--|--|
|rdf-component-dao|基于mybatis-plus接口进行二次封装和配置，包括普通操作和分页等等，简化dao层使用|
|rdf-component-do|定义了实体类基类，提供了共有字段|
|rdf-component-enum|枚举组件，提供了枚举常用的方法，同时支持枚举字段内容自动透出|
|rdf-component-exception|定义了通用的异常类型|
|rdf-component-lock|分布式锁组件，可基于注解实现分布式锁|
|rdf-component-log|基于aop实现日志的打印|
|rdf-component-redis|提供redisson创建配置方法|
|rdf-component-result|封装了统一的响应实体|
|rdf-component-run|提供了项目的启动类即配置|
|rdf-component-user|user模块，提供了用户的基本信息维护，用户登录，注册等接口，实现权限管理等|
|rdf-component-util|提供一些常用的工具类，如Bean克隆复制，加解密，正则验证，雪花算法等工具|
|rdf-component-web|提供了接口异常统一拦截处理|

#### 技术框架
- 基础框架：Spring Boot 2.4.3.RELEASE
- 持久层框架：Mybatis Plus 3.4.2
- 缓存：Redis
- 其他: lombok 1.18.18、fastjson 1.2.76、redisson 3.15.3

#### 开发环境
- 语言：Java 1.8.0_271
- IDE(Java)：IDEA
- 依赖管理：Maven
- 缓存：Redis