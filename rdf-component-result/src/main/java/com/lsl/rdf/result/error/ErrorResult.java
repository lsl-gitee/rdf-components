package com.lsl.rdf.result.error;

import com.lsl.rdf.errCode.ErrorCode;
import com.lsl.rdf.result.BaseResult;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lsl on 2021/6/2.
 */
@SuppressWarnings("rawtypes")
public class ErrorResult extends BaseResult implements Serializable {
    private static final long serialVersionUID = -2336441373804233326L;
    /**
     * 应用名称
     */
    private String application;
    /**
     * 请求url
     */
    private String url;
    /**
     * 响应时间
     */
    private String date;

    public void setApplication(String application) {
        this.application = application;
    }

    public String getApplication() {
        return application;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public ErrorResult(Object data, String subCode, String subMsg) {
        this();
        this.data = data;
        this.subCode = subCode;
        this.subMsg = subMsg;
    }

    public ErrorResult(String subCode, String subMsg) {
        this();
        this.subCode = subCode;
        this.subMsg = subMsg;
    }

    public ErrorResult(String subCode, String subMsg, String msg) {
        this();
        this.subCode = subCode;
        this.subMsg = subMsg;
        this.msg = msg;
    }

    public ErrorResult(ErrorCode errorCode, String msg) {
        this();
        this.subCode = errorCode.getCodeStr();
        this.subMsg = errorCode.getMsg();
        this.msg = msg;
    }

    public ErrorResult(String subMsg) {
        this();
        this.subMsg = subMsg;
    }

    public ErrorResult(ErrorCode errorCode) {
        this();
        this.subCode = errorCode.getCodeStr();
        this.subMsg = errorCode.getMsg();
    }

    public ErrorResult(Object data) {
        this();
        this.data = data;
    }

    public ErrorResult() {
        this.application = "unknown";
        this.url = "unknown";
        this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
        this.success = false;
        this.code = BUSINESS_ERROR_CODE;
        this.msg = "失败";
    }

    @Override
    public String toString() {
        return "ErrorResult{" +
                "application='" + application + '\'' +
                ", url='" + url + '\'' +
                ", date='" + date + '\'' +
                ", id=" + id +
                ", code=" + code +
                ", data=" + data +
                ", subCode='" + subCode + '\'' +
                ", subMsg='" + subMsg + '\'' +
                ", msg='" + msg + '\'' +
                ", success=" + success +
                '}';
    }
}
